import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { HomeComponent } from './home/home.component';
import { CourselistComponent } from './courselist/courselist.component';
import { CoursedetailComponent } from './coursedetail/coursedetail.component';
import { CourseaddComponent } from './courseadd/courseadd.component';
import { CoursedeleteComponent } from './coursedelete/coursedelete.component';
import { CourseupdateComponent } from './courseupdate/courseupdate.component';
import { SearchComponent } from './search/search.component';

const appRoutes: Routes = [
  { path: "", component: HomeComponent },
  { path: "all", component: CourselistComponent},
  { path: "details", component: CoursedetailComponent },
  { path: "add", component: CourseaddComponent},
  { path: "delete", component: CoursedeleteComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CourselistComponent,
    CoursedetailComponent,
    CourseaddComponent,
    CoursedeleteComponent,
    CourseupdateComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule, 
    RouterModule.forRoot(appRoutes), 
    FormsModule, 
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
