import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'coursesProj';
  backgroundvid: string = "../assets/backgroundvid.mp4";
  isMuted: string = " ";
  ourAudio: string = "../assets/audio1.mp3";
  display = "none";
}
