export class Course {
    // {"id":3,"dept":"CompSci","courseNum":201,"courseName":"Angular","instructor":"Rob","startDate":"Sep 9","numDays":15}
    constructor(public id: number,
        public dept: string,
        public courseNum: number,
        public courseName: string,
        public instructor: string,
        public startDate: string,
        public numDays: number)
        {
        }
    
}
