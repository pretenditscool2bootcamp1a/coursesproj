import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Course } from '../models/course.model';
import { CourseCourse } from '../providers/course.service';

@Component({
  selector: 'app-courselist',
  templateUrl: './courselist.component.html',
  styleUrls: ['./courselist.component.css']
})
export class CourselistComponent implements OnInit{
  courses: Array<Course> = [] ;
  constructor(private courseService: CourseCourse, private router: Router){}

  ngOnInit(): void {
    this.courseService.getCourses()
    .subscribe(data=> {
      this.courses = data;
    })
   
  } 
  onClick(id: string){
    console.log("in onclick " + id + "|");
    this.router.navigate(['/details'],
  {queryParams: {
      id: id
  }});
  }

  addCourse(){
    console.log("in addCourse ");
    this.router.navigate(['/add'],
  {queryParams: {
  }});
  }
  
}
