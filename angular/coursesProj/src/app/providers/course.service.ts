import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Course } from '../models/course.model';

@Injectable({
  providedIn: 'root'
})
export class CourseCourse {
  private CoursesEndpoint: string = 'http://localhost:8081/api/courses' ;
  private httpOptions = {
    headers: new HttpHeaders (
      {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin' : "*"
      }
    )
  }

  constructor(private http: HttpClient) { }

  getCourses(): Observable<Course[]> 
  {
    return this.http.get(this.CoursesEndpoint , this.httpOptions)
    .pipe( map (res=> <Course[]>res))
    ;
  }

  getCourseById(CourseID: string): Observable<Course> 
  {
    return this.http.get((this.CoursesEndpoint + "/" + CourseID), this.httpOptions)
    .pipe( map (res=> <Course>res))
    ;
  }

  getCoursesByCategory(CatId: string): Observable<Course[]> 
  {
    return this.http.get((this.CoursesEndpoint + "/bycategory/" + CatId), this.httpOptions)
    .pipe( map (res=> <Course[]>res))
    ;
  }


}
