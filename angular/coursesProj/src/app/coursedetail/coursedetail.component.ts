import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Course } from '../models/course.model';
import { CourseCourse } from '../providers/course.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-coursedetail',
  templateUrl: './coursedetail.component.html',
  styleUrls: ['./coursedetail.component.css']
})
export class CoursedetailComponent {
  course!: Course  ;
  id: string = "";

  constructor(private activatedRoute: ActivatedRoute, private courseCourse: CourseCourse){}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params["id"];
      console.log ("catch passed param" + this.id) ;
      });
    this.courseCourse.getCourseById(this.id)
    .subscribe(data=> {
      this.course = data;
    })
    
  }
}
